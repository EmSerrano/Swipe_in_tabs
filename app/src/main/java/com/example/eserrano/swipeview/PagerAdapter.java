package com.example.eserrano.swipeview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

/**
 * Created by E.serrano on 6/1/18.
 */

public class PagerAdapter extends FragmentStatePagerAdapter
// Esta clase se usa cuando no tienes definido cuantos fragmentos utizaras(tabs) o puedes utilizar el FragmentPaperAdpater.
{
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int numOfTabs) //se agrego una variable entera para contar las tabs.
    {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) //metodo para agregar las posiciones de cada una de las tabs.
    {
        switch (position)
        {
            case 0:
                tab1 t1 = new tab1();
                return t1;
            case 1:
                tab2 t2 = new tab2();
                return  t2;
            case 2:
                tab3 t3 = new tab3();
                return t3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() //con este metodo obtienes el numero de tabs.
    {
        return mNumOfTabs;
    }

}
